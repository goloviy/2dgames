﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Load : MonoBehaviour
{
    [SerializeField] private Image LoadingImg;
    [SerializeField] private Text progressText;
    float progress;   

    private void Start()
    {
        progress = 0;
    }
    void Update()
    {
        LoadingImg.fillAmount = progress;
        progress = progress + 0.01f;
        if (progress < 1)
        {
            progressText.text = string.Format("{0:0}%", progress * 100);
        }
        if(progress == 1f)
        {
            progress = 1f;
            progressText.text = string.Format("{0:0}%", progress * 100);
        }
    }
}
