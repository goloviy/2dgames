﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour{
   
    [SerializeField] private GameObject[] spritePlatforms;
    [SerializeField] private int lenghtPlatform;
    [SerializeField] private int numberPlatforml;    
    private float sizePixel = 1.28f;
     
    [ContextMenu("Greate platform")]
    private void GreateObject()
    {
        do
        {           
            for (int i = 0; i < lenghtPlatform; i++)
            {
                if (i == 0)
                {                  
                   GreatePlatform(0, i, numberPlatforml);  
                }

                if (i > 0 && i < lenghtPlatform - 1)
                {
                    GreatePlatform(Random.Range(1,3), i,numberPlatforml);
                }

                if (i == lenghtPlatform - 1)
                {
                    GreatePlatform(spritePlatforms.Length - 1, i,numberPlatforml);
                }
            }
            numberPlatforml--;
        }
        while (numberPlatforml > 0);
    }

    private void GreatePlatform(int numberSrite,int iterator, int corY)
    {
       Instantiate(spritePlatforms[numberSrite], new Vector3(iterator * sizePixel, corY, 0), Quaternion.identity);
    }

}
