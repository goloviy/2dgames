﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxAndCoins : MonoBehaviour
{
    [SerializeField] GameObject coinPrefab;
    private int numberCoins = 5;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D boxCollider2D;

    private void Start()
    {
        spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        boxCollider2D = gameObject.GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {        
        var player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {            
            StartCoroutine(CreateCoins());
            if (numberCoins == 0)
            {
                boxCollider2D.enabled = false;
                Destroy(transform.GetChild(0).gameObject);
            }
        }                
    }

    private IEnumerator CreateCoins()
    {
        numberCoins--;
        if (numberCoins >= 0)
        {
            GameManager.Coins++;
            var coins = Instantiate(coinPrefab, transform.position, Quaternion.identity);
            var timer = 1.5f;
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, spriteRenderer.color.a - 0.2f);
            while (timer > 0f)
            {
                coins.transform.Translate(Vector2.up * Time.deltaTime);
                timer -= Time.deltaTime;
                yield return null;
            }
            Destroy(coins);            
        }
    }
}
