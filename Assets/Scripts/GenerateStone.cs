﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateStone : MonoBehaviour
{
    [SerializeField] GameObject stonePrefab;
    private float timer;

    void Start()
    {
        timer = Random.Range(1f, 10f);
    }
    void Update()
    {
        timer -= Time.deltaTime;       
        if (timer <= 0f)
        {
            Instantiate(stonePrefab, transform.position, Quaternion.identity);
            timer = Random.Range(1f, 10f);
            Debug.Log(timer);
        }
    }    
}
