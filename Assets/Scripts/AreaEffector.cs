﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaEffector : MonoBehaviour
{
    private AreaEffector2D areaEffector2D;
    void Start()
    {
        areaEffector2D = gameObject.GetComponent<AreaEffector2D>();       
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var stone = other.gameObject.GetComponent<Stone>();
        if (stone != null)
        {
            int rnd = Random.Range(-1, 2);              
            Debug.Log("Работает");
            switch (rnd)
            {
                case -1:
                    areaEffector2D.forceAngle *= -1;
                    Debug.Log($"Рандом = {rnd}");
                    areaEffector2D.forceMagnitude *= -1;
                    break;
                case 0:
                    Debug.Log("Выкинуло 0");
                    break;
                case 1:
                    areaEffector2D.forceAngle *= 1;
                    Debug.Log($"Рандом = {rnd}");
                    areaEffector2D.forceMagnitude *= 1;
                    break;
                default:
                    break;
            }   
        }
    }
}
