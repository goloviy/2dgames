﻿using System;
using UnityEngine;
using System.Collections;


public class InputManager : MonoBehaviour
{
    public static float HoriZontalAxis;

    public static event Action<float> JumpAction;
    public static event Action<string> FireAction;

    private float jumpTimer;
    private Coroutine waitJumpCoroutine;
    

    private void Start()
    {
        HoriZontalAxis = 0;
    }

    private void Update()
    {
        HoriZontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            if (waitJumpCoroutine == null)
            {
                waitJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }

            jumpTimer = Time.time;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }

        if(Input.GetButtonDown("Fire2"))
        {
            FireAction?.Invoke("Fire2");
        }
    }

    private IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.2f);
        if (JumpAction != null)
        {
            var force = Time.time - jumpTimer <= 0.2f ? 1.5f : 1f; // TODO убарть в константы
            JumpAction.Invoke(force);
        }

        waitJumpCoroutine = null;
    }
}
