﻿using System;
using System.Collections;
using UnityEngine;

    public class TestCoroutine : MonoBehaviour
    {
        private void Start()
        {
            Debug.Log("Start");
            StartCoroutine(WaitAndPrint("Coroutine"));
        }

        private IEnumerator WaitAndPrint(string txt)
        {
            yield return new WaitForSeconds(2f);
            Debug.Log(txt);
        }
    }