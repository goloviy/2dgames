﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{
    [SerializeField] int damage;
    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.gameObject.GetComponent<Player>();             
        if (player != null)
        {
            Debug.Log("Попал в игрока.");
            player.Hit(damage);
            Destroy(gameObject);
        }
    }
}
